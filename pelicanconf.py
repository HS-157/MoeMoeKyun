#!/usr/bin/env python3
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# À propos
AUTHOR = 'HS-157'
SITENAME = "HS-157.moe moe kyun !"
SITESUBTITLE = "Je suis un chanceux parmi les uns, et un malheureux parmi les autres..."
SITEURL = 'https://calepin.hs-157.moe'

TIMEZONE = "Europe/Paris"
DEFAULT_LANG = 'fr'
LOCALE = "fr_FR.utf8"
DATE_FORMATS = {'fr': '%d/%m/%y',}

PATH = 'content'
RELATIVE_URLS = True
REVERSE_CATEGORY_ORDER = True
DEFAULT_PAGINATION = False

DEFAULT_CATEGORY = ''

# Gestion des liens
SLUGIFY_SOURCE = 'title'
ARTICLE_URL = '{category}/{slug}'
ARTICLE_SAVE_AS = '{category}/{slug}/index.html'
PAGE_PATHS = ['pages']
PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}/index.html'
CATEGORY_URL = 'c/{slug}'
CATEGORY_SAVE_AS = 'c/{slug}/index.html'
TAG_URL = 't/{slug}'
TAG_SAVE_AS = 't/{slug}/index.html'

# Auteur
AUTHOR_URL = ''
AUTHOR_SAVE_AS = ''
AUTHORS_SAVE_AS = ''

# Flux RSS / ATOM
FEED_ALL_ATOM = "atom/all.xml"
FEED_ALL_RSS = "rss/all.xml"
CATEGORY_FEED_ATOM = None
# CATEGORY_FEED_ATOM = "atom/c/{slug}.xml"
# CATEGORY_FEED_RSS = "rss/c/{slug}.xml"
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
# TAG_FEED_ATOM = "atom/t/{slug}.xml"
# TAG_FEED_RSS = "rss/t/{slug}.xml"
TRANSLATION_FEED_ATOM = None
TRANSLATION_FEED_RSS = None


# Liens
LINKS = (('Benvii', "http://www.benvii.com/"),
         ('Guigui Show', "http://www.guiguishow.info/"),
         ('Chouhartem', "https://blog.epheme.re/"),
         ('H2L29', "http://h2l29200.tk/"),
         ('Maison du Libre', "http://mdl29.net/"),
         ('Pelican', "http://blog.getpelican.com/"),)

# Social
SOCIAL = (('Mastodon', 'https://framapiaf.org/@HS_157'),
          ('Twitter', 'https://twitter.com/HS_157'))

STATIC_PATHS = ['static']

EXTRA_PATH_METADATA = {
    'static/robots.txt': {'path': 'robots.txt'},
    'static/typhlo.png': {'path': 'typhlo.png'},
    'static/favicon.ico': {'path': 'favicon.ico'}
}


# Configuration thème
# THEME = 'SVBHACK'

USER_LOGO_URL = "typhlo.png"
USE_GENERIC_FONT_FAMILIES = True
