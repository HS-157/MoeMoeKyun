Title: Installer un serveur DHCP sur Archlinux avec DHCPD
Date: 2016-11-10 16:39
Author: HS-157
Category: Informatique
Tags: Gnu/Linux, Archlinux, DHCP, Libre

Aujourd'hui on va faire du DHCP pour faire un sous-réseau avec [DHCPD](https://en.wikipedia.org/wiki/DHCPD) qui est le serveur DHCP fait par l'[ISC](https://www.isc.org/) qui est l'[Internet Systems Consortium](https://fr.wikipedia.org/wiki/Internet_Systems_Consortium).

On va partir du principe qu'on a deux interfaces réseaux sur notre machine, la première *eno1* qui est connecté à internet et *eno2* qui va être notre sous-réseau. Le paquet sous Archlinux est *[dhcp](https://www.archlinux.org/packages/?name=dhcp)* avec qui, il ne faut pas confondre avec *[dhcpcd](https://www.archlinux.org/packages/?sort=&q=dhcpcd&maintainer=&flagged=)* qui est le client DHCP qui permet de recevoir une IP.

Avant tout on choisit la plage d'adresse, pour moi ça sera du 10.0.0.0/27 car du 10.0.0.0 c'est facile à retenir et /27 car on peut avoir environ 30 machines dessus et c'est suffisant pour mon sous-réseau.

Il faut commencer par se mettre une IP à notre interface réseau, car DHCPD va s'acrocher à celle qui aura une IP dans le bloc du subnet.
~~~bash
sudo ip link set up dev eno2
sudo ip addr add 10.0.0.1/27 dev eno2
~~~

Maintenant on peut commencer à faire notre fichier de configuration pour DHCPD.
~~~
option domain-name-servers # Serveur DNS
		80.67.169.12,      # FDN
		80.67.169.40,      # FDN
		89.234.141.66,     # ARN
		80.67.188.188;     # LDN
		89.234.186.18	   # GRIFON

option ntp-servers         # Serveur NTP
		ntp-p1.obspm.fr,
		canon.inria.fr;

authoritative;             # On en a besoin pour qu'il soit légitime sur le réseau

subnet 10.0.0.0 netmask 255.255.255.224 {
		option routers             # Tout ce qu'il faut pour faire le réseau
				10.0.0.1;          #
		option subnet-mask         #
				255.255.255.224;   #
		option broadcast-address   #
				10.0.0.31;         #

		range 10.0.0.10 10.0.0.30; # Plage d'ip à attribuer
}
~~~

Il suffit de lancer le service maintenant
~~~bash
sudo systemctl enable dhcpd4.service
sudo systemctl start dhcpd4.service
~~~

Normalement vous devez avoir des ip sur l'interface réseau qu'on avait choisi, mais le problème, c'est qu'on a pas accès à l'internet car il y a pas de liaison entre les deux interfaces. Pour faire ça, il faut activer le *forwarding* et du NAT entre ces deux interfaces avec Iptable.

Pour activer le **forwarding** on peut faire un ```sudo sysctl net.ipv4.ip_forward=1``` mais au prochain reboot, plus rien, pour garder il faut éditer le fichier ```/etc/sysctl.d/30-ipforward.conf``` :
~~~
net.ipv4.ip_forward=1
net.ipv6.conf.default.forwarding=1
net.ipv6.conf.all.forwarding=1
~~~

Ensuite le NAT entre nos deux interfaces.
~~~
sudo iptables -t nat -A POSTROUTING -o eno1 -j MASQUERADE
sudo iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i eno2 -o eno1 -j ACCEPT
~~~

Et paf ! Normalement on a un sous-réseau tout bien, tout propre avec l'accès à l'internet du mode !

Mais il y a toujours un problème, c'est qu'on a pas IP sur l'interface qu'il faut et que les règles iptables ne reste pas non plus.

Avoir mettre automatiquement une ip, on va créer un service SystemD qu'on pourra mettre dans ```/etc/systemd/system/network.service```.
~~~
[Unit]
Description=Network Connectivity
Wants=network.target
Before=network.target
BindsTo=sys-subsystem-net-devices-eno2.device
After=sys-subsystem-net-devices-eno2.device
  
[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/sbin/ip link set dev eno2 up
ExecStart=/sbin/ip addr add 10.0.0.1/27 broadcast 10.0.0.31 dev eno2
ExecStop=/sbin/ip addr flush dev eno2
ExecStop=/sbin/ip link set dev eno2 down
    
[Install]
WantedBy=multi-user.target
~~~

Il faut modifier les choses qu'il faut pour que ça marche chez vous et après on peut l'activer pour le prochain démarrage.
~~~bash
sudo systemctl enable network
~~~

Et pour sauvegarder dans ```/etc/iptables/iptables.rules```.
~~~
# iptables-save > /etc/iptables/iptables.rules
~~~

Et il y a juste à activer le service.
~~~bash
sudo systemctl enable iptables
~~~

Et repaf ! Normalement tout dois marcher et automatiquement !

Références :

- [DHCPD](https://wiki.archlinux.org/index.php/Dhcpd)
- [Internet sharing - Enable packet forwarding](https://wiki.archlinux.org/index.php/Internet_sharing#Enable_packet_forwarding)
- [Internet sharing - Enable NAT](https://wiki.archlinux.org/index.php/Internet_sharing#Enable_NAT)
- [Le Guigui Show](http://www.guiguishow.info/2016/10/12/tp-mobilite-et-reseaux-sans-fil-reseau-sans-fil-securise-et-monitore-mobilite-ipv6/#toc-5560-dhcp)
- [Liste de serveur DNS](http://dukeart.netlib.re/dokuwiki/doku.php/dns:server)
- [Connexision au réseau - Réseau statique](https://wiki.archlinux.fr/Connexions_reseau#R.C3.A9seau_statique)
- [Iptables - Installation](https://wiki.archlinux.fr/Iptables#Installation)
