Title: Généner une pair de clefs SSH
Date: 2016-05-29 02:31
Author: HS-157
Category: Informatique
Tags: Gnu/Linux, Archlinux, SSH, Libre

Générer une pair de clefs ssh c'est simple, avec la commande suivante :
```
ssh-keygen -t rsa -b 8192 -C "$(whoami)@$(hostname)-$(date -I)"
```

Que fait cette commande ? Regardons un peu les options :  

* *-t* permet de choisir le type de chiffrement, ici du rsa
* *-b* c'est la taille de cette clef, ici 8 192 bits mais on peut monter jusqu'à 16 384 bits avec du rsa
* *-C* permet de rajouter des commentaires, ici avec qui on est, la machine où on est et la date du jour sous la forme AAAA-MM-JJ

Après pour exporter facilement la clef publique sur le serveur que vous voulez, vous pouvez utiliser cette commande :
```
ssh-copy-id -i ~/.ssh/id_rsa.pub -p port utilisateur@serveur
```

Aussi, si vous voulez pas vous amusez à taper une commande à ralonge de ce style ```ssh -p port -i ~/.ssh/id_rsa utilisateur@serveur``` à chaque fois, vous pouvez créer un fichier de config dans ```~/.ssh/config``` et mettre ceci.
~~~
Host nom_machin
HostName serveur
User utilisateur
Port port
IdentityFile ~/.ssh/id_rsa
~~~

Références :

- [SSH - Clé publique/privée](https://wiki.archlinux.fr/Ssh#Cl.C3.A9_publique.2Fpriv.C3.A9e)
- [SSH keys - Generating an SSH key pair](https://wiki.archlinux.org/index.php/SSH_keys#Generating_an_SSH_key_pair)
- [SSH : Authentification par clé - Configuration par utilisateur](http://doc.fedora-fr.org/wiki/SSH_:_Authentification_par_cl%C3%A9#Configuration_par_utilisateur)
