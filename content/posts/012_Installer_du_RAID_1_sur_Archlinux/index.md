Title: Installer du RAID 1 sur Archlinux
Date: 2016-11-26 03:10
Author: HS-157
Category: Informatique
Tags: Gnu/Linux, Archlinux, RAID, Libre

Ici, aujourd'hui, on va mettre en place simplement du RAID 1 logiciel avec mdadm. On part de base qu'on a deux disques où on veut faire du mirroir tout simple sans installer de système dessus. Donc il faut commencer par avoir [mdadm](https://www.archlinux.org/packages/?name=mdadm) d'installé.

Il faut commencer par créer la grappe qui sera /dev/md0
~~~
sudo mdadm --create --verbose --level=1 --metadata=1.2 --raid-devices=2 /dev/md0 /dev/sdb /dev/sdc
~~~

La commande est composé du RAID qu'on veut (--level=1), du nombre de disque (--raid-devices=2), des disques et du nom de la grappe.

Après, il faut attendre que le RAID se contruise, et on en a pour un petit moment.

Et quand c'est fini, vous pouvez formater le nœud au format que vous voulez.

Et pour que le système sache qu'il y a du RAID, il faut mettre dans le fichier de configuration :
~~~
mdadm --detail --scan >> /etc/mdadm.conf
~~~

Et voilà, du RAID comme il faut.

Pour savoir si le RAID est toujours en vie, ça sera pour plus tard, mais la documention d'Ubuntu-fr sur le [RAID logiciel](https://doc.ubuntu-fr.org/raid_logiciel#utilisationconfiguration) à l'air d'être bien pour ça.

Références :

- [RAID - Archlinux](https://wiki.archlinux.org/index.php/RAID)
- [RAID - Archlinux.fr](https://wiki.archlinux.fr/RAID)
- [Raid logiciel (mdadm)](https://wiki.debian-fr.xyz/Raid_logiciel_(mdadm))
