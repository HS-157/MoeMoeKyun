Title: Monter un VPN coté client avec OpenVPN sur Archlinux
Date: 2016-11-11 14:19
Author: HS-157
Category: Informatique
Tags: Gnu/Linux, Archlinux, VPN, Libre

Aujourd'hui on va monter un VPN coté client avec OpenVPN et petit bonus, ça sera la suite de l'article sur le [serveur DHCP]({filename}/2016-11-10_Installer_un_serveur_DHCP_sur_Archlinux_avec_DHCPD.md).

On va partir du principe qu'on a comme interface réseau *eno1* qui est internet, *eno2* qui est notre sous-réseau et on aura un *tun0* qui sera notre VPN. On part aussi du principe qu'on a déjà un serveur VPN quelque part, ici ça sera un VPN fournit par [GRIFON](https://grifon.fr/).

Tout d'abord, il faut installer le paquet, ici c'est *[openvpn](https://www.archlinux.org/packages/?name=openvpn)* et ensuite il faut modifier le fichier de configuration.  
Ici, nous avons un [fichier de configuration](https://wiki.grifon.fr/doku.php?id=reseau:openvpn#fichier_de_conf_client) ainsi qu'une clef partagé, tout les deux fournit par GRIFON, ils se place dans le dossier ```/etc/openvpn/```. On va y mettre le fichier de configuration qui va s'appeller par exemple ```grifon.conf``` et notre clef partagé qui va être ```ta.key```.

Il y a plus qu'a lancer le service, mais avec SystemD, le nom du service est ```openvpn@<configuration>.service```, donc pour ici c'est :
~~~bash
sudo systemctl enable openvpn@grifon.service
sudo systemctl start openvpn@grifon.service
~~~

Là il va vous demander votre indentifiant et votre mot de passe fournit par le serveur OpenVPN, pour ne pas le taper, vous pouvez rajouter un ```auth-user-pass $file``` avec *$file* un nom de fichier avec dedans l'identifiant et mot de passe dans le fichier de configuration.

Là on a notre VPN qui est monté avec tout le trafic de la machine de dans, mais comme nous avons un sous-réseau, on veut que tout passe dedans et tout ça grace à iptables.
~~~bash
sudo iptables -t nat -A POSTROUTING -o tun0 -j MASQUERADE
sudo iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i eno2 -o tun0 -j ACCEPT
~~~

Et pour sauvegarder dans ```/etc/iptables/iptables.rules```.
~~~
# iptables-save > /etc/iptables/iptables.rules
~~~

Et il y a juste à activer le service.
~~~bash
sudo systemctl enable iptables
~~~

Et là, normalement tout passe dedans.

Références :

- [OpenVPN - SystemD service configuration](https://wiki.archlinux.org/index.php/OpenVPN#systemd_service_configuration)
- [SwardArMor](https://www.swordarmor.fr/)
