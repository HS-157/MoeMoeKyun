Title: Configurer son serveur SSH
Date: 2016-11-14 01:42
Author: HS-157
Category: Informatique
Tags: Gnu/Linux, Archlinux, SSH, Libre

Là, c'est avoir de bonne option pour notre serveur SSH, une liste et pourquoi :

- Choisir le port du serveur
	- ```Port XXXXX```
- Utilise la version 2 du protocole SSH
	- ```Protocol 2```
- Évite la connexion root
	- ```PermitRootLogin no```
- Permet l'audentification par clef SSH
	- ```PubkeyAuthentication yes```
- Désactive la connexion par mot de passe
	- ```PasswordAuthentication no```
- Désactive les mots de passe vide
	- ```PermitEmptyPasswords no```
- Désactive la connexion par le biais du module PAM
	- ```UsePAM no```
- Déactive *"challenge-response"* de PAM
	- ```ChallengeResponseAuthentication no```
- Déactive le transfert d'agent
	- ```AllowAgentForwarding no```
- Déactive le transfert TCP
	- ```AllowTcpForwarding no```
- Déactive le tranfert de X11
	- ```X11Forwarding no```
- Limite le nombre de tentatives de connexion, ici au bout de 2 erreurs, la probabilité de refuser la connexion est de 50% et monte linéairement jusqu'à 100% au bout de 5 connexions
	- ```MaxStartups 2:50:5```
- Restreint l'accès aux utilisateurs indiqués uniquement
	- ```AllowUsers <utilisateur>```
- Restreint l'accès aux groupes indiqués uniquement
	- ```AllowGroups <groupe>```
- Interdire l'accès à certains utilisateur
	- ```DenyUsers test guest admin root snort apache nobody```
- Interdire l'accès à certains groupes
	- ```DenyGroups test guest admin root snort apache nobody```
- Ne pas chercher le nom associé à une IP (Reverse DNS)
	- ```UseDNS no```
- Maintenir une session inactive en vie
	- ```ClientAliveInterval 120```

Exemple de mon fichier de configuration.
~~~
#	$OpenBSD: sshd_config,v 1.99 2016/07/11 03:19:44 tedu Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/bin:/bin:/usr/sbin:/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

Port XXXXX
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::

# The default requires explicit activation of protocol 1
Protocol 2

# HostKey for protocol version 1
#HostKey /etc/ssh/ssh_host_key
# HostKeys for protocol version 2
#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_dsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
#HostKey /etc/ssh/ssh_host_ed25519_key

# Lifetime and size of ephemeral version 1 server key
#KeyRegenerationInterval 1h
#ServerKeyBits 1024

# Ciphers and keying
#RekeyLimit default none

# Logging
#SyslogFacility AUTH
#LogLevel INFO

# Authentication:

#LoginGraceTime 2m
PermitRootLogin no
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

#RSAAuthentication yes
PubkeyAuthentication yes

# The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
# but this is overridden so installations will only check .ssh/authorized_keys
AuthorizedKeysFile	.ssh/authorized_keys

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#RhostsRSAAuthentication no
# similar for protocol version 2
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# RhostsRSAAuthentication and HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
PasswordAuthentication no
PermitEmptyPasswords no

# Change to no to disable s/key passwords
ChallengeResponseAuthentication no

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no

# GSSAPI options
#GSSAPIAuthentication no
#GSSAPICleanupCredentials yes

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
UsePAM no
ChallengeResponseAuthentication no

AllowAgentForwarding no
AllowTcpForwarding no
#GatewayPorts no
X11Forwarding no
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes
PrintMotd no # pam does that
#PrintLastLog yes
#TCPKeepAlive yes
#UseLogin no
#UsePrivilegeSeparation sandbox
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
#ClientAliveCountMax 3
#UseDNS no
#PidFile /run/sshd.pid
MaxStartups 2:50:5
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# override default of no subsystems
Subsystem	sftp	/usr/lib/ssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#	X11Forwarding no
#	AllowTcpForwarding no
#	PermitTTY no
#	ForceCommand cvs server

AllowUsers hs-157
AllowGroups users

DenyUsers test guest admin root snort apache nobody
DenyGroups test guest admin root snort apache nobody

UseDNS no
ClientAliveInterval 120
~~~

Pour générer une pair de clefs SSH, c'est par [ici]({filename}/2016-05-29_Généner_une_pair_de_clefs_SSH.md)

Références :

- [SSH](https://wiki.archlinux.fr/Ssh)
- [Secure Shell](https://wiki.archlinux.org/index.php/Secure_Shell)
- [Hardening OpenSSH](https://dev.gentoo.org/~swift/docs/security_benchmarks/openssh.html)
