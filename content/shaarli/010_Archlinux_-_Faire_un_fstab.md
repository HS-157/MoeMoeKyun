Title: [Shaarli] Archlinux - Faire un fstab
Date: 2016-12-18 07:18
Author: HS-157
Category: Shaarli
Tags: Shaarli, Libre, Archlinux, Gnu/Linux, Informatique

# [Archlinux - Faire un fstab](https://gist.github.com/Brainiarc7/c2dfc75ce931491fe510)

Si vous voulez pas vous amusez à faire bien le fstab, sous Archlinux, il y a un script qui permet de faire ça qu'on utilise lors de l'installation.

Il est dans le paquet [arch-install-scripts](https://www.archlinux.org/packages/extra/any/arch-install-scripts/) et c'est *genfstab*.
